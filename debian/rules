#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

STEM = less

DOCS = README.md

# generate manpage with help2man from --help option of Node.js script
_mkman = NODE_PATH=lib \
 help2man $(patsubst %,--name %,$3) --no-info --output $2 $1 \
 || { NODE_PATH=lib $1 --help; false; }

%:
	dh $@

# optimize JavaScript for browser use
debian/js/%.min.js: debian/js/%.js
	uglifyjs --compress --mangle \
		--comments /Copyright/ \
		--output $@ \
		-- $<

# pre-compress for browser use
%.gz: %
	pigz --force --keep -11 -- $<
	brotli --force --keep --best -- $<

override_dh_auto_build: \
 debian/js/$(STEM).min.js.gz

# build browser library and documentation
# * concatenate libraries for browser use
# * generate manpage from --help option of script unless nodoc requested
debian/js/less.js:
	mkdir --parents debian/js
	sed \
	-e s/@VERSION@/$(DEB_VERSION_UPSTREAM_REVISION)/ \
	-e s/@YEAR@/$(shell egrep -m1 -o '^[0-9]{4}' CHANGELOG.md)/ \
	< debian/jsheader.in.txt > $@~
	echo '(function (window, undefined) {' >> $@~
	cat $(shell perl debian/jspaths.pl build/build.yml) >> $@~
	echo '})(window);' >> $@~
	mv -f $@~ $@
	$(call _mkman, bin/lessc, debian/lessc.1, \
		compiles LESS - a.k.a. Leaner Style Sheets - to CSS)

# Run testsuite
override_dh_auto_test:
	nodejs test

override_dh_installdocs:
	dh_installdocs --all -- $(DOCS)

.SECONDARY:
